#!/bin/bash 


az login

echo "resource group name:"
read rgName
echo $rgName

echo "plan name:"
read planName
echo $planName

echo "storage account name"
read storageName
echo $storageName

echo "Azure function app"
read azFunApp
echo $azFunApp



#---------------------------------------------------------------------------------------------------------------------------------
#creating the resource group
az group create --name $rgName
#---------------------------------------------------------------------------------------------------------------------------------
#creating a plan

az appservice plan create \
  --name $planName \
  --resource-group $rgName \
  --is-linux \
  --sku S1
#---------------------------------------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------------------------------------
#creating the storage account

az storage account create \
  --name $storageName \
  --resource-group $rgName \
  --sku Standard_LRS

#---------------------------------------------------------------------------------------------------------------------------------
# creating the function app

az functionapp create \
  --name $azFunApp \
  --resource-group $rgName \
  --storage-account $storageName \
  --functions-version 4 \
  --plan $planName \
  --runtime "dotnet" \
  --runtime-version 6 \
  --functions-version 4 
#----------------------------------------------
#create aslot
az functionapp deployment slot create --name $azFunApp --resource-group $rgName --slot test
